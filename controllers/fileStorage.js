'use strict';

var fs = require('fs'),
    mkdirOrig = fs.mkdir,
    path = require('path'),
    crypto = require('crypto'),
    osSep = path.sep,
    transliteration = require('transliteration'),
    async = require('async'),
    mime = require('mime');

function rename(file, dest, callback) {
    var newFileName = transliteration(path.normalize(file.originalname).split('.')[0], '_').split(' ').join('_') + '_' + crypto.createHash('md5').update(file.name + new Date().toISOString()).digest('hex').substring(0, 5) + path.extname(file.originalname);
    fs.rename(file.path, process.cwd() + '/u' + dest + newFileName, function(err) {
        if (err) {
            console.log(err);
            callback(err);
        } else {
            callback(null, {
                file: 'http://static.acrm.io' + dest + newFileName,
                sPath: '/u' + dest + newFileName
            });
        }
    });
}

function mkdir_p(path, callback, position) {
    var parts = require('path').normalize(path).split(osSep);

    position = position || 0;

    if (position >= parts.length) {
        return callback(null);
    }

    var directory = parts.slice(0, position + 1).join(osSep) || osSep;
    fs.stat(directory, function(err) {
        if (!err) {
            mkdir_p(path, callback, position + 1);
        } else {
            mkdirOrig(directory, function(err) {
                if (err && err.code !== 'EEXIST') {
                    return callback(err);
                } else {
                    mkdir_p(path, callback, position + 1);
                }
            });
        }
    });
}

exports.uploadFile = function(req, res) {
    if (req.files.length) {
        async.each(req.files, function(file, callback) {
            if (file.size < 52428800) {
                console.log('Processing file', file);
                var dest = '/' + new Date().getFullYear() + '/' + ((new Date().getMonth() + 1) < 10 ? '0' + (new Date().getMonth() + 1) : (new Date().getMonth() + 1)) + '/' + (new Date().getDate() < 10 ? '0' + new Date().getDate() : new Date().getDate()) + '/';
                var path = process.cwd() + '/u' + dest;
                if (!fs.existsSync(path)) {
                    mkdir_p(path, function(err) {
                        rename(file, dest, function(err, savedFile) {
                            if (err) {
                                return res.status(500).send(err);
                                callback(err);
                            } else
                                res.jsonp(savedFile);
                        });
                    });
                } else {
                    rename(file, dest, function(err, savedFile) {
                        if (err) {
                            res.status(500).send(err);
                            callback(err);
                        } else
                            res.jsonp(savedFile);
                    });
                }
            } else {
                res.status(406).send('File size exceeds the limit (50Mb)');
                callback('File size exceeds the limit (50Mb)');
            }
        }, function(err) {
            if (err) {
                console.log('Error while processing file', err);
            }
        });
    } else
        return res.status(400).send('Files are missing');
};

exports.removeFile = function(req, res) {
    if (!req.query || !req.query.sPath)
        return res.status(400).send('Bad request');
    fs.unlink(process.cwd() + req.query.sPath, function(err) {
        if (err) {
            console.log(err);
            return res.status(500).send(err);
        } else
            return res.status(200).send();
    });
};

exports.uploadAvatar = function(req, res) {
    function makeRandSuf() {
        var text = '';
        var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (var i = 0; i < 5; i += 1)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }

    function saveAvatar(avatar, userId, cb) {
        if (avatar) {
            var base64Data = avatar.replace(/^data:image\/png;base64,/, '');
            mkdir_p(process.cwd() + '/u/avatars/' + userId + '/', function(err) {
                if (err) {
                    console.log(__filename, err);
                    cb(err);
                } else {
                    var fileName = makeRandSuf() + '.png',
                        fullImgPath = process.cwd() + '/u/avatars/' + userId + '/' + fileName;
                    fs.writeFile(fullImgPath, base64Data, 'base64', function(err) {
                        if (err) {
                            console.log(__filename, err);
                            cb(err);
                        } else {
                            cb(null, 'http://static.acrm.io/avatars/' + userId + '/' + fileName);
                        }
                    });
                }
            });
        } else {
            cb();
        }
    }

    var avatar = req.body.avatar,
        user = req.body.user;
    if (!avatar || !user)
        return res.status(400).send('Bad request');

    saveAvatar(avatar, user, function(err, img) {
        if (err) {
            console.log('Error while saving avatar', err);
            return res.status(500).send(err);
        } else {
            console.log('avatar saved', img);
            return res.jsonp({
                avatar: img
            });
        }
    });
};
