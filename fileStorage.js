var express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    multer = require('multer'),
    errorHandler = require('errorhandler'),
    methodOverride = require('method-override'),
    hostname = process.env.HOSTNAME || 'localhost',
    port = parseInt(process.env.PORT, 10) || 3003,
    publicDir = process.argv[2] || __dirname + '/u',
    fileStorage = require('./controllers/fileStorage.js'),
    upload = multer({
        dest: './.tmp/',
        limits: {
            fileSize: 52428800
        }
    }).array('file');

app.use(bodyParser.json());

app.use(methodOverride());

app.use(function(req, res, next) {
    console.log(req.method, ' ' + req.hostname + req.path);
    next();
});

app.use(express.static(publicDir));

app.use(errorHandler({
    dumpExceptions: true,
    showStack: true
}));

app.get('/', function(req, res) {
    res.redirect('/view/index.html');
});

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(cors());

app.post('/api/uploadFile', function(req, res, next) {
    upload(req, res, function(err) {
        if (err) {
            console.log('Error while uploading file', err);
            return res.status(500).send(err);
        } else
            next();
    });
}, fileStorage.uploadFile);

app.delete('/api/removeFile', fileStorage.removeFile);

app.post('/api/uploadAvatar', fileStorage.uploadAvatar);

console.log('Static file server showing %s listening at http://%s:%s', publicDir, hostname, port);
app.listen(port, hostname);
